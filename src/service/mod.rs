pub mod dev_refresh;

#[cfg(feature = "serverside")]
pub mod serverside {
    use axum::Router;

    #[allow(clippy::let_and_return)]
    pub fn build_routes<S: Clone + Send + Sync + 'static>() -> Router<S> {
        let router = Router::new();
        let router = super::dev_refresh::serverside::build_routes(router);
        router
    }
}

//! When the server is restarted, its client side needs to refresh
//!
//! The usual recommended way to organize a codebase is to put same things
//! together, so all the forks go in one drawer, and all the components
//! in a `components` modules, etc.
//!
//! I like to put code that works together, together.
//!
//! You will notice that there is still a `service` module, but that's an
//! indication that what's inside needs a smart server, rather than an
//! organization tool.
//!
//! By smart server, I mean that `src/bin/server.rs` needs to add routes for
//! everything in this module, and all of the components in the `service`
//! module cannot work properly without this specific implementation or a code
//! change.

use crate::prelude::*;

/// Server side route for whatever this service needs that exists only on the
/// server.
const ROUTE: &str = "/api/dev-refresh-token";

/// State keeping for the client side component.
///
/// The following docstrings will describe how this type progresses inside
/// the function called `poll_token()` to make the component itself do its work
#[derive(Debug)]
enum RefreshState {
    /// The component starts off `Initial`ized the first time it is "mounted".
    ///
    /// That's a yew concept and you can read more on their website, but that
    /// essentially means that it happens on the first time the component
    /// executes.
    Initial,

    /// then it sends the first HTTP `GET` request to `ROUTE`,
    /// and become in `FirstFlight`.
    FirstFlight,

    /// When the request has a response, it's an `i32` and the component
    /// is idle, and remembers that response if it's the first.
    ///
    /// If the response and the remembered token are different, that means
    /// the server has changed, and the component needs to refresh the page
    Idle(i32),

    /// When it's time to send another request, it is sent, but the component
    /// still remembers the first token.
    Flight(i32),
}

#[function_component]
pub fn Component() -> Html {
    let state = use_state(|| RefreshState::Initial);

    {
        let state = state.clone();
        use_mount(move || {
            wasm_bindgen_futures::spawn_local(poll_token(state));
        })
    };

    use_interval(
        move || {
            wasm_bindgen_futures::spawn_local(poll_token(state.clone()));
        },
        1000,
    );
    html! {}
}

/// change component states, send a request, and on a new response, refresh
/// the browser's page.
///
/// Whenever `state.set()` is called, the new state is queued up in yew, which
/// will eventually call the component's function again with a new state
/// returned on `use_state()`, and that is why we need the in-flight states,
/// and why we must ignore them
async fn poll_token(state: UseStateHandle<RefreshState>) {
    match *state {
        RefreshState::Initial => state.set(RefreshState::FirstFlight),
        RefreshState::Idle(token) => state.set(RefreshState::Flight(token)),
        _ => {
            // a request is in progress, do not send any more
            return;
        }
    }

    match gloo::net::http::Request::get(ROUTE).send().await {
        Ok(token) => {
            if let Ok(token) = token.json::<i32>().await {
                match &*state {
                    RefreshState::Initial => state.set(RefreshState::Idle(token)),
                    RefreshState::Idle(previous_token) => {
                        if token != *previous_token {
                            // refresh the page
                            gloo::utils::window().location().reload().ok();
                        }
                        state.set(RefreshState::Idle(token))
                    }
                    _ => state.set(RefreshState::Initial),
                }
            } else {
                state.set(RefreshState::Initial);
            }
        }
        Err(_) => {
            state.set(match &*state {
                RefreshState::Initial | RefreshState::FirstFlight => {
                    // The server has never responded, so whenever it's back,
                    // this page should refresh.
                    RefreshState::Idle(0)
                }
                RefreshState::Idle(token) | RefreshState::Flight(token) => {
                    RefreshState::Idle(*token)
                }
            });
        }
    }
}

#[cfg(feature = "serverside")]
pub mod serverside {
    use super::ROUTE;
    use axum::{extract::State, response::IntoResponse, routing::get, Router};

    /// Axum uses the router to figure out which async function needs to be
    /// called based on the request's URL. Here we register `super::ROUTE` to
    /// a small function that has an `i32` as state, and responds with that
    /// number.
    ///
    /// A little trick that Axum pulls: `S` in `Router<S>` means what state is
    /// **missing**, and not the intuitive "what's inside"
    pub fn build_routes<S: Clone + Send + Sync + 'static>(router: Router<Token>) -> Router<S> {
        let mut token = rand::random();
        // never generate zero as the token when it never received a response
        // to deal with the edge case where a client side starts working AFTER
        // the server is closed.
        if token == 0 {
            token = 1;
        }

        router
            .route(ROUTE, get(get_refresh_token))
            .with_state(Token(token))
    }

    /// Axum considers anything that is `Clone`able as possible state.
    ///
    /// We already made it into a state in `build_ruotes()` in the call to
    /// `with_state()`
    #[derive(Debug, Clone, Copy)]
    pub struct Token(i32);

    pub async fn get_refresh_token(State(token): State<Token>) -> impl IntoResponse {
        serde_json::to_string(&token.0).unwrap()
    }

    /// Testing Axum is not too hard, but I have no idea how to test the
    /// client side :(
    #[cfg(test)]
    mod tests {
        use super::*;
        use axum::{
            body::{Body, HttpBody},
            http::Request,
        };
        use tower::Service;

        /// poll the body to its end, no matter its length, and return a vec
        /// of bytes.
        ///
        /// This is not internet safe because there are no limit checks
        async fn to_bytes<T>(body: T) -> Vec<u8>
        where
            T: HttpBody,
            T::Data: IntoIterator<Item = u8>,
        {
            futures::pin_mut!(body);
            let mut res = vec![];
            while let Some(Ok(bytes)) = body.data().await {
                res.extend(bytes)
            }
            res
        }

        #[tokio::test]
        async fn token_is_stable_in_one_router() {
            let mut app = build_routes(Router::new());

            // first response
            let req = Request::builder().uri(ROUTE).body(Body::empty()).unwrap();
            let res = app.call(req).await.unwrap();
            let body = to_bytes(res.into_body()).await;
            let token1 = serde_json::from_slice::<i32>(&body).unwrap();

            // second response
            let req = Request::builder().uri(ROUTE).body(Body::empty()).unwrap();
            let res = app.call(req).await.unwrap();
            let body = to_bytes(res.into_body()).await;
            let token2 = serde_json::from_slice::<i32>(&body).unwrap();

            assert_eq!(token1, token2);
        }

        #[tokio::test]
        async fn token_is_different_in_two_routers() {
            // first response
            let mut app = build_routes(Router::new());
            let req = Request::builder().uri(ROUTE).body(Body::empty()).unwrap();
            let res = app.call(req).await.unwrap();
            let body = to_bytes(res.into_body()).await;
            let token1 = serde_json::from_slice::<i32>(&body).unwrap();

            // second response
            let mut app = build_routes(Router::new());
            let req = Request::builder().uri(ROUTE).body(Body::empty()).unwrap();
            let res = app.call(req).await.unwrap();
            let body = to_bytes(res.into_body()).await;
            let token2 = serde_json::from_slice::<i32>(&body).unwrap();

            assert_ne!(token1, token2);
        }
    }
}

use web_sys::HtmlInputElement;

use crate::prelude::*;

#[derive(Debug, PartialEq, Properties)]
pub struct Props {
    pub value: String,
    pub oninput: Callback<String>,
}

#[function_component]
pub fn Component(props: &Props) -> Html {
    let value = props.value.clone();

    let oninput = {
        let oninput = props.oninput.clone();
        Callback::from(move |ev: InputEvent| {
            // the following javascript ugly does not need to exist in a utils
            // module, because this is the **only** component that will ever take
            // a string from the value.
            //
            // Change this component to support whatever it's missing for your
            // usecase
            if let Some(new_name) =
                (move || Some(ev.target()?.dyn_into::<HtmlInputElement>().ok()?.value()))()
            {
                oninput.emit(new_name);
            }
        })
    };

    html! {
        <input {value} {oninput} />
    }
}

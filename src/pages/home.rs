use crate::prelude::*;

#[function_component]
pub fn Component() -> Html {
    let name = use_state(|| "yew".to_string());
    let oninput = {
        let name = name.clone();
        Callback::from(move |new_name| name.set(new_name))
    };

    html! {
        <div class="modal is-active">
            <div class="modal-background"></div>
            <div class="modal-content">
                <div class="box">
                    <div class="media">
                        <div class="media-content">
                            <p>{"Enter Your Name"}</p>
                            <text_input::Component value={name.to_string()} {oninput}/>
                            <p>
                                {"Hello, "}{&*name}
                                <span class="icon"><i class="fas fa-hand-spock"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}

//! The client-side of a [yew.rs] website
//!
//! Make sure you have wasm32-unknown-unknown
//!
//! ```text
//!     $ rustup target add wasm32-unknown-unknown
//! ```

use yew_ssr_template::ClientApp;

use wasm_bindgen::prelude::wasm_bindgen;

#[wasm_bindgen(main)]
fn main() {
    yew::Renderer::<ClientApp>::new().hydrate();
}

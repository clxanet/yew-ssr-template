//! The deployer is a set of commands that help this template prepare its server
//!
//! The server needs some files to properly handle client requests.
//! Specifically it needs a wasm file, its javascript glue, and
//! various assets.
//!
//! Use `prepare` for that.
//!
//! ```text
//! [alias]
//! prepare = run --bin deployer -- prepare
//! ```
//!
//! which defaults to the debug build of the client side at
//! `target/wasm32-unknown-uknown/debug/`
//!
//! To prepare a release build of the client, try
//!
//! ```text
//! $ cargo run --bin deployer -- prepare target/wasm32-unknown-unknown/release/
//! ```
//!

use std::{env::Args, path::PathBuf};
use tracing_subscriber::prelude::*;

fn main() {
    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::try_from_default_env().unwrap_or_else(|_| "info".into()),
        )
        .with(tracing_subscriber::fmt::layer())
        .init();

    let mut args = std::env::args();
    let _exe = args.next().expect("missing argv[0]: executable name");
    match args
        .next()
        .expect("missing argv[1]: path to index.html")
        .as_str()
    {
        "prepare" => prepare(args),
        command => println!("Unknown command: {command}"),
    }
}

const DEFAULT_WASM: &str = "target/wasm32-unknown-unknown/debug/";

fn prepare(mut args: Args) {
    let assets = args.next().expect("missing argv[2]: path to assets");
    let target = PathBuf::from(args.next().unwrap_or(DEFAULT_WASM.to_string()));

    let pkg = target.join("pkg");
    let wasm = target.join("clientside.wasm");

    let mut command = std::process::Command::new("wasm-bindgen");

    command
        .arg("--target=web")
        .arg(&wasm)
        .arg("--out-dir")
        .arg(&pkg);

    println!(
        "$ wasm-bindgen {}",
        command
            .get_args()
            .map(|a| a.to_string_lossy())
            .collect::<Vec<_>>()
            .join(" ")
    );

    if !command.status().unwrap().success() {
        println!("Failed to wasm-bindgen on {}", wasm.to_string_lossy());
        std::process::exit(1);
    }

    copy_dir(assets.into(), pkg);
}

fn copy_dir(from: PathBuf, to: PathBuf) {
    for item in std::fs::read_dir(&from).unwrap() {
        let item = item.unwrap();
        let path = item.path();

        let relative = path.strip_prefix(&from).unwrap();
        let new_to = to.join(relative);

        if item.metadata().unwrap().is_file() {
            println!("cp {} {}", path.to_string_lossy(), new_to.to_string_lossy());
            std::fs::copy(path, new_to).unwrap();
        } else {
            println!("mkdir {}", new_to.to_string_lossy());
            std::fs::create_dir_all(&new_to).unwrap();
            copy_dir(path.to_owned(), new_to);
        }
    }
}

//! A server to test the website on a developer's machine
//! or to dual-host a yew website and a backend server.

use tower_http::services::ServeDir;
use yew_ssr_template::{service, ServerApp, ServerAppProps};

use axum::{
    body::StreamBody,
    extract::{Query, State},
    handler::HandlerWithoutStateExt,
    http::{Method, Uri},
    response::IntoResponse,
    routing::get,
    Server,
};
use futures::stream::{self, StreamExt};
use std::{collections::HashMap, convert::Infallible};
use tracing_subscriber::prelude::*;

#[cfg(debug_assertions)]
const DEFAULT_PKG: &str = "target/wasm32-unknown-unknown/debug/pkg/";

#[cfg(not(debug_assertions))]
const DEFAULT_PKG: &str = "target/wasm32-unknown-unknown/release/pkg/";

#[tokio::main(flavor = "current_thread")]
async fn main() {
    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::try_from_default_env().unwrap_or_else(|_| "info".into()),
        )
        .with(tracing_subscriber::fmt::layer())
        .init();

    let mut args = std::env::args();
    let _exe = args.next().expect("missing argv[0]: executable name");
    let index = args.next().expect("missing argv[1]: path to index.html");
    let assets = args.next().unwrap_or(DEFAULT_PKG.to_string());

    // try to render a yew page
    let svc = get(render)
        .with_state(make_state(&index).await)
        .into_service();

    // but before that, try to serve assets
    let svc = ServeDir::new(assets)
        .append_index_html_on_directories(false)
        .fallback(svc);

    // buf first of all, this application gets a try at the request
    let app = service::serverside::build_routes();

    println!("You can view the website at: http://localhost:8080/");
    Server::bind(&"127.0.0.1:8080".parse().unwrap())
        .serve(app.fallback_service(svc).into_make_service())
        .await
        .unwrap();
}

async fn render(
    url: Uri,
    method: Method,
    Query(queries): Query<HashMap<String, String>>,
    State((index_html_before, index_html_after)): State<(String, String)>,
) -> impl IntoResponse {
    let url = url.to_string();
    tracing::info!(?method, ?url, "fallback to render yew index");

    let renderer = yew::ServerRenderer::<ServerApp>::with_props(move || ServerAppProps {
        url: url.into(),
        queries,
    });

    StreamBody::new(
        stream::once(async move { index_html_before })
            .chain(renderer.render_stream())
            .chain(stream::once(async move { index_html_after }))
            .map(Result::<_, Infallible>::Ok),
    )
}

async fn make_state(path: &str) -> (String, String) {
    let index_html_s = tokio::fs::read_to_string(path)
        .await
        .expect("failed to read index.html");

    let (index_html_before, index_html_after) = index_html_s
        .split_once("<!-- server side rendering split point -->")
        .unwrap();

    (index_html_before.to_string(), index_html_after.to_string())
}

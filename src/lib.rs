pub mod components;
pub mod pages;
pub mod service;

pub mod prelude {
    pub use crate::{components::*, Route};
    pub use stylist::{style, yew::styled_component};
    pub use wasm_bindgen::prelude::*;
    pub use yew::prelude::*;
    pub use yew_hooks::prelude::*;
    pub use yew_router::prelude::{Link, Redirect};
}

use std::collections::HashMap;

use yew_router::history::{AnyHistory, History, MemoryHistory};
use yew_router::prelude::*;

use prelude::*;

#[function_component]
pub fn ClientApp() -> Html {
    html! {
        <BrowserRouter>
            <App />
        </BrowserRouter>
    }
}

#[derive(Properties, PartialEq, Eq, Debug)]
pub struct ServerAppProps {
    pub url: AttrValue,
    pub queries: HashMap<String, String>,
}

#[function_component]
pub fn ServerApp(props: &ServerAppProps) -> Html {
    let history = AnyHistory::from(MemoryHistory::new());
    history
        .push_with_query(&*props.url, &props.queries)
        .unwrap();

    html! {
        <Router history={history}>
            <App />
        </Router>
    }
}

#[function_component]
fn App() -> Html {
    html! {
        <>
            if cfg!(debug_assertions) {
                <service::dev_refresh::Component />
            }
            <Switch<Route> render={switch} />
        </>
    }
}

#[derive(Routable, PartialEq, Eq, Clone, Debug)]
pub enum Route {
    #[not_found]
    #[at("/404")]
    NotFound,

    #[at("/")]
    Home,
}

fn switch(route: Route) -> Html {
    match route {
        Route::NotFound => {
            html! { <h1>{"404 - Not Found"}</h1> }
        }
        Route::Home => {
            html! {
                <pages::home::Component />
            }
        }
    }
}

# yew-ssr-template

Template website powered by [yew.rs] with server-side rendering.

Includes [bulma.io] for styling, and [fontawesome.com] to let you play with a large set of icons.

## Getting started

Install Rust, and add a browser target

```
    $ rustup target add wasm32-unknown-unknown
```

Install wasm-bindgen-cli

```
    $ cargo install -f wasm-bindgen-cli
```

Now you can look at the `.cargo/config.toml` file and see aliases to build the client wasm, prepare a development serve directory, and host yew website.

if you have `cargo-watch` installed, you can use `cargo xwatch` to run all
three steps on each source-code change.

otherwise, you need to run on each change

```
    $ cargo build-client
    $ cargo prepare
    $ cargo serve
```


# Diving Deeper

A yew website with server-side rendering takes your code and prepares an HTML document from it. That document is sent to the client, and has the client ask for more files, including a wasm file it can run to provide interactiviy.


## Client side

Yew supports a lot of interactivity, which is done using javascript and WebAssembly. The client side binary needs to be compiled and copied to a package directory before a user can see it.

Building the client side is a two step process. First, you need to have rust build a wasm file:

```
$ cargo build --bin clientside --target=wasm32-unknown-unknown --no-default-features
```

This creates a wasm file, but in the current state of the web, to use wasm you need some javascript glue. That's what wasm-bindgen is for:

```
$ wasm-bindgen --target=web target/wasm32-unknown-unknown/debug/clientside.wasm --out-dir target/wasm32-unknown-unknown/debug/pkg
```

The first part is handled by `cargo build-client` alias, and the second one
is handled by our little `deployer` bin. Specifically it's `prepare` command.

## Misc. Files

These are all the files in /asset that help your site be prettier than the web's defaults.

The server sends only files in the package directory, so `deployer`s `prepare` command also does that.

## The Server

All of these files need to be sent to the client using a specific protocol. That's what the `server` is for.

The server also does the server side rendering to help clients without javascript show *something*, so it needs a scaffold we call `index.html`

The server will first try to serve a bakend route. If it does not match, it will try to serve one of the files in the package directory, and if there is no file with that name, it will serve a Yew page.

Ultimately, the Yew page decides if a URL does not exist.

# Fullstack example

This template demonstrates a "fullstack service" - code written entirely in Rust, that provide a simple feature called dev-refresh.

## The Feature

When the server is restarted, its client side needs to refresh.

## Server side

The server will pick a random number, and serve it to any `GET /api/dev-refresh-token` requests.

## Client side

The client will `GET /api/dev-refresh-token` on first execution, and once per second.

## Implementation

See `src/service/dev_refresh.rs` for details. `src/lib.rs` includes the `DevRefresh` component in its `App()` in all debug builds to enable the functionality on the client side,
and we add a `build_routes()` function to enable it on the server side.